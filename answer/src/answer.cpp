#include <ros/ros.h>
//#include <geometry_msgs/Point.h>
#include <visualization_msgs/Marker.h>

class SubscribeAndPublish
{
public:
    SubscribeAndPublish()
{
    pub_ = n_.advertise<visualization_msgs::Marker>("output", 1);

    marker.header.frame_id = "/my_frame";
    marker.header.stamp = ros::Time::now();
    marker.ns = "basic_shapes";
    marker.id++;
    marker.type = visualization_msgs::Marker::POINTS;
    marker.action = visualization_msgs::Marker::ADD;

    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;

    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    marker.scale.x = 1.0;
    marker.scale.y = 1.0;
    marker.scale.z = 1.0;

    marker.color.r = 1.0;
    marker.color.g = 0.0;
    marker.color.b = 0.0;
    marker.color.a = 1.0;
    
    sub_ = n_.subscribe("input", 1, &SubscribeAndPublish::callback, this);
}

void callback(const geometry_msgs::Point& input)
{
    //while (ros::ok()){
        marker.points.push_back(input);
        if (marker.points.size() > 4) {
            pub_.publish(marker);
            marker.points.clear();
        }
    //}
 }

private:
    ros::NodeHandle n_; 
    ros::Publisher pub_;
    ros::Subscriber sub_;
    visualization_msgs::Marker marker;
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "subscribe_and_publish");
    SubscribeAndPublish SAPObject;
    ros::spin();
    return 0;
}
